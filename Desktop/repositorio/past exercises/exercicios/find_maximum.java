/*Write a Java program to read an array of integers and display the maximum value that it contains.

Sample Input:

{ 1, 20, 2, 5, 7 }
Sample Output:
max: 20
*/

import java.util.*;
public class find_maximum {
    public static void main (String[] args) {  
    
        System.out.println(" choose the size of the array ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int array[] = new int [size];
        
        int biggerNumber = Integer.MIN_VALUE;
        
        System.out.println("insert " +size+ " numbers");
        for(int i = 0; i < array.length; i++){
           
            array[i] = scanner.nextInt();
        }
        
        
        for (int i = 0 ; i < array.length ; i++) {
            
            if (array[i] > biggerNumber) {
                biggerNumber = array[i];
            }
        }
        System.out.println("Biggest: " + biggerNumber);
        
    
    
    
    }
}         