import java.util.Scanner;

public class Arvoredenatal {
    public static void main (String[] args) {

        System.out.println("altura da arvore");
        Scanner teclado = new Scanner(System.in);
       
        int nivel = teclado.nextInt();
        for (int linha = 0; linha <= nivel; linha++) {
            for (int coluna = -1*nivel; coluna<= nivel; coluna++) {
                if(linha >= Math.abs(coluna) ){
                    System.out.print("*");
                }else{
                    System.out.print("-");
                }                
            }
            System.out.print("\n");
        }
    }
}