import java.util.*;

public class Gravitycalculator{
    public static void main(String[]args) {
        
        double gravity = 9.81;
        System.out.println("insert fall time on seconds");
        Scanner scanner = new Scanner(System.in);
        double fall = scanner.nextDouble();
        System.out.println("insert initial velocity");
        double velocity = scanner.nextDouble();
        System.out.println("insert initial position");  
        double position = scanner.nextDouble();
        Double xt = 0.5 * gravity * (fall*fall) +  velocity * fall + position ;
        System.out.println("result:" + xt);
        scanner.close();

    }
}