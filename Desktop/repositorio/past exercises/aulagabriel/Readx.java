import java.util.*;

public class Readx{
    public static void main (String[]args) {
        System.out.println("choose how much numbers you desire to the smallest");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        
        int smallNumber = Integer.MAX_VALUE;
        
        for (int i = 0 ; i < x ; i++) {
            int v = scanner.nextInt();
            if (v < smallNumber) {
                smallNumber = v;
            }
        }
        System.out.println("smallest: " + smallNumber);
        scanner.close();
    
    }        
}