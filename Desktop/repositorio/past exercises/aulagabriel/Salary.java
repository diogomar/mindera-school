import java.util.*;

public class Salary{
    public static void main(String[]args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many hours did you have in the week?");
        double hoursofwork = scanner.nextDouble();
        double minsalary = 8;
        double maxhours = 60;
        double extra=0;
        
        System.out.println("how much do you receive per hour?");
        double receivesalary = scanner.nextDouble();
        if(hoursofwork>maxhours){
            System.out.println("get another job");
            return;
        }
        
        if (hoursofwork>40){
            extra=hoursofwork-40;
            hoursofwork -= extra;
        }
        
        double result = receivesalary*hoursofwork;
        System.out.println("do you will receive"+result);
        scanner.close();
    }
}
