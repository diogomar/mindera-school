 import java.util.*;

public class FigureCalculations {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please insert the cube's side: \n");
        int arestaCubo = scanner.nextInt();

        // Volume do cubo = aresta ^ 3
        int volumeCubo = arestaCubo * arestaCubo * arestaCubo;

        System.out.print("Please insert the pyramid's base: \n");
        int basePiramide = scanner.nextInt();
        System.out.print("Please insert the pyramid's height: \n");
        int alturaPiramide = scanner.nextInt();

        scanner.close();

        // calcular o volume da pirâmide (p_base * p_altura)/3 = v_piramide
        int volumePiramide = (basePiramide * alturaPiramide) / 3;
        // ( (5 * 100)/3 )

        System.out.println(volumeCubo == volumePiramide);
        System.out.println(volumeCubo > volumePiramide);
    }
}
