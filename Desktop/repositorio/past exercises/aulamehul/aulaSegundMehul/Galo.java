import java.util.Scanner;
public class Galo{
    public static void main(String[]args){
        
        boolean vitoria = false;
        int tamanho_tabuleiro = 3;
        int casa;
        int casaslivres = tamanho_tabuleiro*tamanho_tabuleiro;
        Scanner keyboard = new Scanner(System.in);
        
        //array bidimensional
        
        String array[][]=new String [tamanho_tabuleiro][tamanho_tabuleiro];
        
        for (int l=0; l<array.length; l++) {
            for (int c=0; c<array[0].length; c++) {
                array[l][c]= " ";
            }
        }
        
        System.out.print("Qual é o 1º jogador (x ou o) ?");
        String jogador = keyboard.nextLine();
        
        while (!vitoria &&(casaslivres>0)) {
            boolean jogada_valida = true;
            
            System.out.print("introduza o número da casa que quer jogar ");
            casa = keyboard.nextInt();
            
            casa = casa - 1;
            
            int linha = casa % tamanho_tabuleiro;
            int coluna = casa / tamanho_tabuleiro;
            
            //validar jogada
            if (array [linha] [coluna] == " ") {
                array [linha] [coluna] = jogador;
                casaslivres = casaslivres-1;
            }
            else {
                jogada_valida = false;
            }
            
            for (int l = 0; l < tamanho_tabuleiro; l++) {
            	String line = "", sep = "";
                for (int c = 0; c < tamanho_tabuleiro; c++) {
                	line += " " + array [c][l] + " ";
                	if (l < tamanho_tabuleiro - 1) sep += "---";
                	if (c < tamanho_tabuleiro - 1) {
                		line += "|";
                		if (l < tamanho_tabuleiro - 1) sep += "+";
                	}
                }
                System.out.println(line);
                System.out.println(sep);
            }
            
            //trocar de jogador
            if (jogada_valida) {
                //condições de vitória
                int d1_t_count = 0, d2_t_count = 0;
                for (int l = 0; l < tamanho_tabuleiro; l++) {
                    int col_t_count = 0, line_t_count = 0;
                    for (int c = 0; c < tamanho_tabuleiro; c++) {
                        // Horizontais e verticais
                        if (array [l][c].equals(jogador)) { col_t_count++; };
                        if (array [c][l].equals(jogador)) { line_t_count++; };
                    }
                    if (col_t_count == tamanho_tabuleiro || line_t_count == tamanho_tabuleiro) { vitoria = true; break; }
                    
                    
                    // Diagonais
                    if (array [l][l].equals(jogador)) { d1_t_count++; }
                    if (array [l][tamanho_tabuleiro - l - 1].equals(jogador)) { d2_t_count++; }
                }
                
                if (d1_t_count == tamanho_tabuleiro || d2_t_count == tamanho_tabuleiro) { vitoria = true; }
                
                if (!vitoria) {
                    if (jogador.equals ("x")) {
                        jogador = "o";
                    }
                    else {
                        jogador = "x";
                    }
                }
            }
        }
        
        if (vitoria) { System.out.println("O jogador " + jogador + " ganhou!"); }
        else { System.out.println("Empate!"); }
    }

    private static String[][] extracted(int tamanho_tabuleiro) {
        String array[][] = new String[tamanho_tabuleiro][tamanho_tabuleiro];
        return array;
    }
}