import java.util.Scanner;

public class Tabuada {
	public static void main(String[] args) {
		int valor;
		Scanner teclado = new Scanner(System.in);

		System.out.println("digite o numero da tabuada que desejas");

		valor = teclado.nextInt();

		for (int i = 0; i <= 10; i++) {
			System.out.println(valor + "x" + i + "=" + (valor * i));
		}
	}
}