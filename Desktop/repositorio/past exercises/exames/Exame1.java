import java.util.*;

public class Exame1 {
    public static void main (String []args){ 
        while(true){ 
            Scanner operador = new Scanner(System.in);
            System.out.println("which operation do you want to perform ? \n"+
            " 1-somar"+
            "\n 2-subtrair \n 3-multiplicar \n 4-dividir \n 5-saida do menu ");
            int opcao = operador.nextInt();
            switch(opcao){
                case 1:
                    soma();
                    break;
                case 2:
                    subtrair();
                    break;
                case 3:
                    multiplicar();
                    break;
                case 4:
                    dividir();
                    break;
                case 5:return;
                
            
            }  
            
        }
    }    
    public static void soma(){
        System.out.println("== Soma ==");
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("what the first number ?");
        int num1 = scanner.nextInt();
        
        System.out.println("what your second number ?");
        int num2 = scanner.nextInt();

        int result = num1 + num2;     
    
        System.out.println("result:" + result);
        
        return;
    }
    
    public static void subtrair(){
        System.out.println("== Subtracao ==");
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("what the first number ?");
        int num1 = scanner.nextInt();
        
        System.out.println("what your second number ?");
        int num2 = scanner.nextInt();

        int result = num1 - num2;     
    
        System.out.println("result:" + result);
    
        return;
    }
    
    public static void multiplicar(){
        System.out.println("== Multiplicar ==");

        Scanner scanner = new Scanner(System.in);
        
        System.out.println("what the first number ?");
        int num1 = scanner.nextInt();
        
        System.out.println("what your second number ?");
        int num2 = scanner.nextInt();

        int result = num1 * num2;     
    
        System.out.println("result:" + result);
        
        return;
    }    

    public static void dividir(){
        System.out.println("== Divisao ==");

        Scanner scanner = new Scanner(System.in);
        
        System.out.println("what the first number ?");
        int num1 = scanner.nextInt();
        
        System.out.println("what your second number ?");
        int num2 = scanner.nextInt();

        int result = num1 / num2;     
    
        System.out.println("result:" + result);

        return;
    }



}