import java.util.*;


public class Stats {

    public static double total (int[] numbersArr){
        double total = numbersArr[0]+numbersArr[1]+numbersArr[2]+numbersArr[3];
        return total ;
    } 
    
    public static double media (int[] numbersArr){
        double media = total(numbersArr) / numbersArr.length;
        return media;
    }
    
    public static double max (int [] numbersArr){
        double max = Double.MIN_VALUE;
        return max;
    }

    public static double min (int [] numbersArr){
        double min = Double.MAX_VALUE;
        return min;
    }

    public static double diff (int [] numbersArr){
        double diff = max(numbersArr)-min(numbersArr);
        return diff;
    }
    
    
    public static void main(String[] args) {
    
        int [] numbersArr = new int[] {2,3,4,5};
        System.out.println(total(numbersArr));
        System.out.println(media(numbersArr));
        System.out.println(max(numbersArr));
        System.out.println(min(numbersArr));
        System.out.println(diff(numbersArr));

    }
}

