import java.util.*;

public class Teste2 {
    public static short FUNCTIONS = 5;

    public static void main(String[] args) {
        double[] statsArray = stats(new int[] {1, 2, 3, 4, 5});
        
        System.out.println("\033\143 " + Arrays.toString(statsArray));
    }

    static double[] stats(int[] numbers) {
        double[] doubleArray = new double[FUNCTIONS];

        doubleArray[0] = sum (numbers);
        doubleArray[1] = media (numbers);
        doubleArray[2] = max (numbers);
        doubleArray[3] = min (numbers);
        doubleArray[4] = difference (numbers);

        return doubleArray;
    }

    static double sum(int[] numbers) {
        double sum = 0;

        for(int i = 0; i < numbers.length; i++)
            sum += numbers[i];
        
        return sum;
    }

    static double media(int[] numbers) {
        double media;

        media = sum(numbers) / numbers.length;

        return media;
    }

    static double max(int[] numbers) {
        double highest = 0;

        for (int i = 0; i < numbers.length; i++)
            if (numbers[i] > highest)
                highest = numbers[i];

        return highest;
    }

    static double min(int[] numbers) {
        double lowest = Integer.MAX_VALUE;

        for (int i = 0; i < numbers.length; i++)
            if (numbers[i] < lowest)
                lowest = numbers[i];

        return lowest;
    }

    static double difference(int[] numbers) {
        double max        = max(numbers);
        double min        = min(numbers);

        double difference = max - min;

        return difference;
    }
}