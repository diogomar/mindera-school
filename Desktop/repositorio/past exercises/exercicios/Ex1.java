import java.util.*;

public class Ex1{
    public static void main (String []args){
        int [][] matrix = {{1,2,5},
                           {3,1,0},
                           {4,9,6},
                           {6,9,5}};


        // [1,2,5]; valor_matriz = 8; soma de todas as outras linhas abaixo desta
        // [3,1,0]; valor_matriz = 4; soma de todas as outras linhas abaixo desta
        int[] sums =  new int[matrix.length];

        // iterate through every row
        for(int row = 0; row < matrix.length; row++){
            sums[row] = 0;
            // calculate row sum
            for(int column = 0; column < matrix[row].length; column++){
                System.out.println("matrix["+row+"]["+column+"] = " + matrix[row][column]);
                sums[row] = sums[row] + matrix[row][column];
                System.out.println("sums["+row+"] = sums["+row+"] + matrix["+row+"]["+column+"]");
                System.out.println(sums[row]);
            }    
        }

        System.out.println("================//==================");

        // -----
        // sums
        // [8, 4, 19, 20]
        //  0  1  2  3
        for(int i = 0; i < sums.length; i++) {
            System.out.println("sums[" + i + "] = " + sums[i]);

        }


        System.out.println("================//==================");

        // reorder lines of matrix and elements of sum
        for(int i = 0; i < sums.length; i++) {
            for(int j = i+1; j < sums.length; j++) {
                if(sums[i] > sums[j]) {
                    // change element in sums
                    int tempSum = sums[i];
                    sums[i]= sums[j];
                    sums[j]= tempSum;

                    // based on sums swaps, change lines of matrix
                    int [] tempArr = matrix[i];
                    matrix[i]= matrix[j];
                    matrix[j]= tempArr;
                }
            }
        }
       
        for(int i = 0; i < sums.length; i++) {
            System.out.println("sums[" + i + "] = " + sums[i]);
        }
       
        // sums = 8, 4, 19, 20



            /*
            // 
            for(int i = x+1; i < matrix.length; i++){
                for(int j = 0; j<matrix[i].length; j++){
                    total_matriz += matrix[i][j];     
                }

                if(total_matriz>valor_matriz){
                    int [] temp = matrix[x];
                    matrix[x]= matrix[i];
                    matrix[i]= temp;
                }

                total_matriz = 0;
            }   
            
            valor_matriz = 0;
        }
        */
        
        /*
        for(int p = 0; p < matrix.length;p++){
            for(int o = 0; o < matrix[p].length;o++){
                System.out.print(matrix[p][o]);    
            }
            System.out.println();
        }
        */
    }
}


/*

 1 2 5 = 8
 3 1 0 = 4
 4 9 6 = 19
 6 9 5 = 20

 8
 4
 19
 20

 20
 19
 8
 4



*/