import java.util.*;

public class Ruben_ex2{
    public static void main (String[]args) {
        System.out.println("choose how much numbers you desire to the Biggest");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt(); 
        int biggestNumber = Integer.MIN_VALUE;
        
        for (int i = 0 ; i < x ; i++) {
            int v = scanner.nextInt();
            if (v > biggestNumber) {
                biggestNumber = v;
            }
        }
        System.out.println("Biggest: " + biggestNumber);
        scanner.close();
    
    }        
}