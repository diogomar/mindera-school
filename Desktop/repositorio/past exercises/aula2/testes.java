public class Salario {
    public static final int MIN_WAGE  = 8;
    public static final int MAX_HOURS = 60;
    public static final int WEEK_HOURS = 40;
    public static final float EXTRA_HOURS_TAX = 1.5f; 

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        float salary = 0;

        System.out.print("Wage /hour: ");
        float wage = scanner.nextFloat();

        System.out.print("\nHours /week: ");
        int hours = scanner.nextInt();
        
        scanner.close();
        
        if (wage < MIN_WAGE) {
            System.out.println("You need to receive at least $8.00/hour");
            return;
        } else if (hours > MAX_HOURS) {
            System.out.println("You can't work more than 60 hours a week");
            return;
        }

        if (hours > WEEK_HOURS) {
            int overtimeHours = hours - WEEK_HOURS;
            salary = (wage * WEEK_HOURS) + (wage * overtimeHours * EXTRA_HOURS_TAX); 
        } else {
            salary = wage * hours;
        }

        System.out.printf("\nYour salary is: %.2f\n", salary);
    }
}