public class Account {
    
    private double balance;

    public Account(double balance){
        this.balance=balance;
    }


    public void whithdraw(double amount){
        this.balance -= amount;
        
    }

    public void deposit (double amount){
        this.balance += amount;
    }

    public void transfer(double amount, Account client){
        if(this.balance >= amount){
            client.deposit(amount);
            this.whithdraw(amount);
        }
    }
    
    public double getBalance(){
        return this.balance;
    }
}